#!/usr/bin/env raku

use v6;
use fatal;

sub is-continuation-line($line) {
  $line && ($line !~~ /^ ['{{' | \s | '「']/);
}

sub paste-lines($lines) {
  my $pending = '';
  my $unclosed = False;
  lazy gather {
    for $lines.Seq -> $line {
      if is-continuation-line($line) || $unclosed {
        $pending ~= $unclosed ?? $line.subst(/^ '{{'/, '') !! $line;
        $unclosed = $line ~~ / '{{' <-[}]>* $ /;
      } else {
        take $pending if $pending;
        $pending = $line;
        take $line if !$line;
        $unclosed = $line ~~ / '{{' <-[}]>* $ /;
      }
    }
    take $pending if $pending;
  }
}

# grammar LEI-E-XION-FMT {
#   token TOP 
# }

sub parse-hacm-str($s) {
  $s.subst(/ '&' (.) /, {"\\harma\{$0\}"}):g
}

sub line-to-tex($line) {
  $line.subst(/'{{' (.*?) '}' '}'?/, { "\\arka\{{parse-hacm-str $0}\}" }):g
    .subst(/^\s+/, '')
}

my @pages;

for "xion_to_txt/second_draft".IO.dir -> $page {
  if $page ~~ / ^ "xion_to_txt/second_draft/xion-" (\d+) ".docx.txt" $ / {
    @pages.push((+$0, $page));
  }
}

@pages .=sort *[0];

my $out = open "src/xion_r.tex", :w;
# my $out = $*OUT;

$out.say(q:to/END/);
\documentclass{src/defs}

\begin{document}
\begin{alignment}
END

for @pages -> ($pn, $page) {
  for paste-lines($page.lines) -> $line {
    next if $line ~~ /^\d+$/;
    if !$line {
      $out.say: '  \blankline';
      next;
    }
    $out.say: '  \begin{edition*}';
    once {
      $out.say: "  \\ekddiv\{type=page,n=$pn,head=\\pagehead\{$pn\}\}";
    }
    $out.say: "  " ~ line-to-tex $line;
    $out.say: '  \end{edition*}';
    $out.say: '  \begin{translation}';
    $out.say: '  \end{translation}';
  }
  $out.say: '';
}


$out.say(q:to/END/);
\end{alignment}
\end{document}
END
