\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{defs}[2021/10/31 Definitions]

\LoadClass[a4paper,12pt]{book}

\RequirePackage{luatexja}
\RequirePackage[haranoaji]{luatexja-preset}
\RequirePackage{libertinus}
\RequirePackage{ekdosis}
\RequirePackage{tabu}
\RequirePackage{amsmath}
\RequirePackage[japanese,french,german,main=english]{babel}
\usepackage[margin=1in]{geometry}
\RequirePackage{hyperref}

\defaultfontfeatures[\rmfamily]{
  Ligatures={Common,Historic},
  CharacterVariant={77,43:1,49},
  Mapping=tex-text,
  StylisticSet={7,10,11},
  Diacritics=MarkToBase
}

\babeladjust{
  autoload.bcp47 = on,
  autoload.bcp47.options = import
}

\babelprovide{chinese}

\ltjsetparameter{jacharrange={-2,-3,-9}}

\newfontfamily{\alblant}[Path=fonts/,Renderer=Harfbuzz]{alblant.ttf}
\newcommand{\textalblant}[1]{{\alblant #1}}

\newfontfamily{\kardinal}[Path=fonts/,Renderer=Harfbuzz]{kardinal.ttf}
\newcommand{\textkardinal}[1]{{\kardinal #1}}

\newfontfamily{\yuuko}[Path=fonts/,Renderer=Harfbuzz]{yuuko.ttf}
\newcommand{\textyuuko}[1]{{\yuuko #1}}

\newcommand{\arka}[1]{\textalblant{#1}}
\newcommand{\harma}[1]{\textyuuko{#1}}

\newcommand{\pagehead}[1]{$\mathfrak{f}$#1.}

\newcommand{\blankline}{\vspace{2em}}

\newcommand{\foreign}[1]{\textit{#1}}
\newcommand{\ctitle}[1]{\textit{#1}}
\newcommand{\kkt}[1]{\textit{\textsc{#1}}}

\newcommand{\lang}[2]{\textit{\foreignlanguage{#1}{#2}}}

\newcommand{\surname}[1]{\textsc{#1}}


