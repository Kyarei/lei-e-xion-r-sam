# lei-e-xion-r-sam

A translation of *[Book of Shion]* by Seren Arbazard into English. I previously worked on an older partial translation of the book but decided to start anew.

Thanks to [sozysozbot’s repository] with a transcription of the text.

[Book of Shion]: http://conlinguistics.org/arka/images/xion.pdf
[sozysozbot’s repository]: https://github.com/sozysozbot/xion_to_txt
