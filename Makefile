TEX=lualatex-dev
TEXFLAGS=-interaction=nonstopmode -halt-on-error
LATEXMK=latexmk
LATEXMKFLAGS=\
	-pdf \
	-pdflatex=lualatex-dev \
	-output-directory=out \
	-quiet \
	-interaction=nonstopmode \
	-latexoption=-halt-on-error

all: out/xion_r.pdf

out/%.pdf: src/%.tex src/defs.cls
	mkdir -p out
	$(LATEXMK) $(LATEXMKFLAGS) -jobname=$* $< || (rm $@; false)

out/images/%.eps: images/%.svg
	mkdir -p out/images
	inkscape -C -E $@ $<

clean:
	rm -rf out

yukkuri:
	@echo "ゆっくりしていってね！"